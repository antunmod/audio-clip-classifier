# Audio clip classifier

This project contains an application for generating audio clips of requested length from input files. The app will be expanded with machine and deep learning files to try and predict to which musical period the given clip belongs

FFmpeg will have to be installed in order to use the application. [Instructions](http://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/)