import math
import random

from constants import ALLOWED_EXTENSIONS


def valid_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def split_name(filename):
    name_split = filename.rsplit(".", 1)
    return name_split[0], name_split[1]


def random_clip_start_end(audio_length, length):
    max_start = math.floor(audio_length - length)
    start = random.randint(0, max_start)
    return start, start + length


def remove_out_folder_prefix(filepath):
    return filepath[7:]
