import service


def trim(audio):
    start_ms = 0
    end_ms = len(audio)
    interval = 100
    silence_threshold = -55
    while start_ms < len(audio) and audio[start_ms: start_ms + interval].dBFS < silence_threshold:
        start_ms += interval

    while end_ms > 0 and audio[end_ms - interval: end_ms].dBFS < silence_threshold:
        end_ms -= interval

    return audio[start_ms:end_ms]


def trim_and_save(filepath):
    audio = service.to_audio_from_file(filepath)
    trimmed = trim(audio)

    filepath_split = filepath.rsplit('/', 1)
    folder_path = filepath_split[0]
    filename_split = filepath_split[1].rsplit(".", 1)
    filename = filename_split[0]
    extension = filename_split[1]

    trimmed.export(f'{folder_path}/{filename}-trimmed.{extension}')
