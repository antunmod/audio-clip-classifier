import io

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.io import wavfile
from tempfile import mktemp

from pydub import AudioSegment

import util


def to_audio_segment(file):
    file_bytes = io.BytesIO(file.read())
    return AudioSegment.from_file(file_bytes)


def to_audio_from_file(file):
    return AudioSegment.from_file(file, 'mp3')


def save_spectrograph(audio, folder_path, name):
    wname = mktemp('.wav')  # use temporary file
    audio.export(wname, format="wav")  # convert to wav
    FS, data = wavfile.read(wname)  # read wav file
    dimensions = len(data.shape)

    if dimensions == 1:
        filename = f'{folder_path}/{name}-1.png'
        plot_and_save_spectrograph(data, FS, folder_path, filename)
    else:
        channels = data.shape[1]
        for i in range(channels):
            filename = f'{folder_path}/{name}-{i}.png'
            plot_and_save_spectrograph(data[:, i], FS, filename)

    return util.remove_out_folder_prefix(filename)


def plot_and_save_spectrograph(data, FS, filename):
    plt.figure(num=None, figsize=(20, 16), dpi=80, facecolor='w', edgecolor='k')
    plt.specgram(data, Fs=FS, NFFT=256, noverlap=128)
    plt.savefig(filename, bbox_inches='tight')
    plt.clf()


def cut_and_save(audio, start, length, folder_path, extension):
    end = start + length
    edited = audio[start * 1000:end * 1000]

    original_path = f'{folder_path}/original.{extension}'
    edited_path = f'{folder_path}/edited.{extension}'
    audio.export(original_path, format="mp3")
    edited.export(edited_path, format="mp3")

    original_download_path = util.remove_out_folder_prefix(original_path)
    edited_download_path = util.remove_out_folder_prefix(edited_path)
    return edited, original_download_path, edited_download_path
