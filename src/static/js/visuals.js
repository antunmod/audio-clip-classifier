$(document).ready(function () {
    $("form#data").submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);

        $("#result").empty().append("Loading...");

        $.ajax({
            url: "audio",
            type: 'POST',
            data: formData,
            success: function (data) {
                $("#result").empty().append(data);
                console.log(data)
            },
            error: function (response) {
                $("#result").empty().append(response.responseText);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
});