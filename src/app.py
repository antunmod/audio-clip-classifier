import os

from flask import Flask, render_template, request, send_file, make_response

import desilencer
import service
import util
from constants import ALLOWED_EXTENSIONS, OUT_FOLDER_PATH

app = Flask(__name__)


@app.route("/")
@app.route("/home")
def test():
    return render_template('home.html', extensions=ALLOWED_EXTENSIONS)


@app.route("/audio", methods=['POST', 'GET'])
def audio():
    if request.method == 'POST':
        file = request.files['file']
        if util.valid_file(file.filename):
            audio = service.to_audio_segment(file)
            audio = desilencer.trim(audio)
            audio_length = len(audio) / 1000
            length = int(request.form['length'])

            if audio_length < length:
                return make_response(render_template('error.html', message=f'Length of file \'{file.filename}\''
                                                                           f'({audio_length}s) is 'f'too short for '
                                                                           f'length \'{length}s\''),
                                     400)
            else:
                directory_exists = True
                while directory_exists:
                    start, end = util.random_clip_start_end(audio_length, length)
                    name, extension = util.split_name(file.filename)
                    folder_path = f'{OUT_FOLDER_PATH}/{name}-{start}-{end}'
                    directory_exists = os.path.exists(folder_path)

                os.mkdir(folder_path)
                edited, original_path, edited_path = service.cut_and_save(audio, start, length, folder_path, extension)

                edited_spectrograph = service.save_spectrograph(edited, folder_path, "edited")
                original_spectrograph = service.save_spectrograph(audio, folder_path, "original")
        else:
            return make_response(render_template('error.html', message=f'Type of file \'{file.filename}\' incorrect'),
                                 400)
    else:
        return make_response(render_template('error.html', message='Get method is not supported'), 400)

    return render_template('result.html', edited=edited_path, original=original_path,
                           edited_spectrograph=edited_spectrograph, original_spectrograph=original_spectrograph)


@app.route("/<folder>/<name>", methods=['GET'])
def download(folder, name):
    return send_file(f'{OUT_FOLDER_PATH}/{folder}/{name}', as_attachment=True)


if __name__ == '__main__':
    app.run(debug=True)
